\chapter{The experiment: setup and instrumentation} \label{sec:experiment} \acresetall
In this chapter the system studied in the experiment and the setup required to measure the desired quantities are described. Moreover, a discussion of the instrumentation devices and their features is also presented. The list of the treated topics includes:
\begin{itemize}
	\item the statement of the goal of present work;
	\item a description of the system under investigation;
	\item an overview of the experimental procedure and setup;
	\item a discussion of the instrumentation employed in the experiment.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Objective}
The goal of this work is to study in detail an experimental method to determine the non-diagonal terms of the \acl{dsf} of monocrystalline silicon.
In fact, in a \ac{cixs} experiment, the \ac{ddcs} (obtained by measurements) depends via quantities that can be determined theoretically (in perfect crystals) on both diagonal and non-diagonal terms of the \ac{dsf}, that can thus be determined.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{The system under investigation}
The physical system under investigation consists of a X-ray beam interacting with a perfect monocrystalline silicon sample.
The theoretical basis for an accurate treatment of the radiation-matter interaction of relevance for \ac{cixs} experiments using perfect crystals is described in \cref{sec:theory}.
A discussion of the components of such physical system is hereby presented, along with a description of the experimental procedure.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The monocrystalline silicon sample}
The sample consists of a monocrystalline silicon block ($\approx 5.0\ \um{cm}$ linear size) asymmetrically cut along the $\miller{1}{1}{1}$ direction, with an asymmetry angle\footnote{More on the asymmetry angle in \cref{sec:theory:dtxrd:formulas}, specifically \cref{fig:asymmetry}.} of $\alpha = 5\ \um{\degree}$ as shown in \cref{fig:sample}.

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=12.0cm]{figures/sample.eps}
 \caption[The monocrystalline silicon sample]{Lateral (left) and 3D (right) views of the monocrystalline silicon sample used for the experiment. Here $\alpha = 5\ \um{\degree}$ and $L = 55\ \um{mm}$. Thin lines indicate (Bragg) lattice planes.}
 \label{fig:sample}
\end{figure}

The crystal lattice structure of silicon can be classified, in Bravais lattice terms, as \Hl{diamond cubic}, i.e. consisting of two interpenetrating \ac{fcc} lattices, or $Fd\bar{3}m$ in terms of space group (more information can be found in \cite{itcA2006}).
Its \hl{conventional unit cell} (or simply \hl{unit cell}) is shown in \cref{fig:Si_unit_cell} and its origin (arbitrary) is traditionally chosen on either one or the other of the following points:
\begin{enumerate}
	\item a silicon atom: this is the standard choice, especially for visualization purposes, since it highlights the underlying Bravais structure;
	\item at $\miller{-\frac{1}{8}}{-\frac{1}{8}}{-\frac{1}{8}}$ of the previous cell, as this is the (or rather a possible) center of symmetry for the unit cell: if an atom is found at a given position, another one will be found at the opposite site.
\end{enumerate}

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=8.0cm]{figures/Si_unit_cell.eps}
 \caption[The unit cell of silicon]{The lattice structure of silicon (diamond cubic), consisting of two interpenetrating \ac{fcc} lattices. The spheres represents $\chem{Si}$ atoms. Note that the origin is taken at a silicon atom, and not in the center of symmetry.}
 \label{fig:Si_unit_cell}
\end{figure}

For the experiments here discussed, there is an important advantage in choosing the origin in the center of symmetry (i.e. second choice), as the \ac{ddcs} equation for a \ac{cixs} experiment gets simplified (see \cref{sec:theory} and \cref{sec:data_analysis:formulas_simplifications} for details).

The asymmetry cut is required for the detection of the signal, since the \ac{cixs} scattered beam would otherwise be absorbed by the sample.
Note that this asymmetry has required the use of little more complicated \ac{dtxrd} formulas (see \cref{sec:theory:dtxrd:formulas}).

Although little or no new information on silicon monocrystals is expected to be gained, the use of such sample has two main advantages:
\begin{enumerate}
	\item the diffraction on perfect crystals is an effective way to produce a coherently modulated wavefield suitable for \ac{cixs} experiments.
	\item silicon is well studied and, therefore: the parameters required for the calculations (e.g. lattice structure, form factors, etc.) are known; some results to be obtained can be compared with those available in the literature.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The X-ray radiation} \label{sec:experiment:x-rays}
X-rays are electromagnetic radiation (photons) in the $\approx \range{10^2}{10^5}\ \um{eV}$ energy range, corresponding to the $\approx \range{10}{0.01}\ \um{nm}$ wavelength range\footnote{The conversion can be performed by combining $E = h \nu$ and $c = \lambda \nu$, where $E$ is the energy, $\nu$ is the frequency, $\lambda$ is the wavelength, $c$ is the speed of light in vacuum and $h$ is the Planck's constant.}.
A summary of electromagnetic radiation spectrum is given in \cref{fig:em_spectrum}.

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=16.0cm]{figures/em_spectrum.eps}
 \caption[Electromagnetic spectrum]{Summary of the electromagnetic radiation spectrum nomenclature and energy / frequency / wavelength logarithmic ($\log_{10}$) scale, particularly focused on the X-ray radiation and the visible light ranges for reference purposes.}
 \label{fig:em_spectrum}
\end{figure}

The most relevant aspect of the X-ray radiation is that its energy corresponds to a wavelength comparable to the length of chemical bonds and crystal lattice parameters.
When an X-ray beam impinges on a crystal, interference effects can then appear (the most relevant of which is the \hl{diffraction}, i.e. elastic scattering constructive interference) that significantly increase the intensity of the scattered radiation in certain directions to the point that it is easily detected.
The study of such scattered radiation carries a large amount of information on the scattering system and actually most of the knowledge on crystal structures has been gained in this way.
Moreover, the scattering process can also be inelastic. In this case the transferred energy determines what excitations can be studied with X-rays, as summarized in \cref{fig:excitation_spectrum}.

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=16.0cm]{figures/excitation_spectrum.eps}
 \caption[X-ray excitations spectrum]{Summary of the excitations spectrum accessible by inelastic X-ray scattering. The \ac{cixs} experiments on silicon of this work are intended to study the excitations in the \quot{plasmons region}.}
 \label{fig:excitation_spectrum}
\end{figure}

It is worthwhile mentioning that the instrumentation dealing with X-ray radiation is generally relatively technologically challenging, inefficient and expensive when compared to similar tools for visible light.
Moreover, X-ray radiation constitutes a health hazard and care must be taken when performing experiments dealing with it.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The \acl{cixs} experiment} \label{sec:experiment:setup}
The main points of these experiments can be summarized, referring also to \cref{fig:cixs_scattering_scheme}, as follows:
\begin{itemize}
	\item a properly manipulated X-ray \ac{sr} beam, defined by $\vect{K}_0$, strikes on a perfect crystal silicon sample, aligned to be in Bragg condition for the lattice point $\vect{g}$;
	\item most of the beam is scattered elastically in the diffraction direction defined by the wavevector $\vect{K}_h$, where a secondary detector measures it;
	\item the forward diffracted and the Bragg diffracted beams interfere to produce a standing wave electromagnetic field;
	\item the standing wave field is inelastically scattered by the silicon crystal;
	\item a convenient direction $\vect{K}_2$ is chosen to measure the inelastically scattered radiation using a spectrometer (this corresponds to a certain exchanged momentum $\vect{q}_0$);
	\item measurements are collected at different energies and for different values of the incident angle around the Bragg one, in order to tune the standing wave condition.
\end{itemize}

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=8.0cm]{figures/cixs_scattering_scheme.eps}
 \caption[The \acs{cixs} scattering scheme]{Schematic view of experimental \ac{cixs} wavevectors. $\vect{K}_0$ is the incident beam, $\vect{K}_h$ is the Bragg reflected radiation for the lattice point $\vect{g}$, $\vect{K}_2$ is the direction of observation (where the detector measuring the \ac{ddcs} is placed) that defines the exchanged momenta $\vect{q}_0$ and $\vect{q}_h$. The relations among such quantities are: $\vect{K}_0 - \vect{K}_2 = \vect{q}_0$, $\vect{K}_h - \vect{K}_2 = \vect{q}_h$, $\vect{K}_h - \vect{K}_0 = \vect{g}$, $\abs{\vect{K}_0} = \abs{\vect{K}_h}$ and $\babs{\vect{q}_0} = \babs{\vect{q}_h}$.}
 \label{fig:cixs_scattering_scheme}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Experimental setup and procedure}
A series of experiments has been performed in order to gain information on the non-diagonal response of silicon via \ac{cixs}.
It is important to define the experimental setup and the procedure followed to achieve such a goal.
All experiments discussed in this work have been performed at beamline \hl{ID16}\footnote{ID is used as abbreviation for \acroletter{I}nsertion \acroletter{D}evice to specify the type X-ray source.} within the \ac{esrf} complex\footnote{Located in Grenoble, France.}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The setup}
The experimental setup used to collect the data is shown in \cref{fig:experimental_setup}.
% An X-ray beam is produced by means of an undulator and then adjusted by means
All the relevant components are discussed in greater detail later in this chapter.

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=16.0cm]{figures/expt_setup_scheme.eps}
 \caption[The experimental setup]{Schematic side view of the experimental setup (not in scale). The beam, produced by the electrons accelerated in the undulator, is filtered by the slits and subsequently collimated by the \ac{crl}. Then, a monochromator is used to select the energy of the beam that impinges on the sample. The Bragg reflected radiation is measured by the photodiode detector, while the \ac{ddcs} signal is analyzed using a spherically bent crystal and collected by an \ac{apd} in single-photon mode.}
 \label{fig:experimental_setup}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The procedure}
The data collection procedure for the experiments is quite simple and it can be summarized as follows:
\begin{itemize}
	\item \Hl{sample alignment}: a certain number of reflections are used to first determine the lattice orientation and then rotate the sample so that incident beam, sample, diffracted beam of a selected reflection and secondary detector (measuring the diffracted beam) all lay in the same plane;
	\item \Hl{detectors positioning}: the primary detector is positioned in order to fix the exchanged momentum $\vect{q}_0$ of the inelastically scattered beam;
	\item \Hl{signal measurements}: the intensity of the \ac{ddcs} for the \ac{cixs} experiment at the primary detector is measured.
	\item \Hl{background measurements}: the analyzer is misaligned (on purpose), and the same measure as before are repeated.
\end{itemize}

The whole procedure is then repeated for different positions of the spectrometer (tuning of \ac{cixs} exchanged momentum $\vect{q}_0$) and sample (choice of $\vect{g}$, which determines $\vect{q}_h$).

Each measurement, for both signal and background, consists of the following steps:
\begin{enumerate}
	\item \Hl{alignment}: determine the absolute angular position of the selected reflection (using secondary detector) and center at its maximum.
	\item \Hl{angular scans}: perform small rotations of the sample (angular departure, i.e. deviation from Bragg's angle) around the total reflectivity range and collect signal from both detectors.
	\item \Hl{energy scans}: modify the incident energy (which causes the exchanged one to be modified correspondingly, the final energy being fixed) and repeat the procedure from the first step.
\end{enumerate}

It has to be observed that the signal from both detectors is required: the first detector measures the \ac{ddcs} signal, while the second detector measurements are used for alignment purposes of the calculated wavefield amplitudes and phase shift, as explained in greater detail in \cref{sec:data_analysis}.

The resulting collected data can be stored (with some preprocessing) into a matrix where, for example, rows corresponds to energy scans and columns to angular departure scans.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{X-ray instrumentation}
The laboratory where all experiments took place, \ac{esrf}, is a modern third-generation\footnote{Works for an upgrade of the center are in progress.} synchrotron and it includes devices to produce (sources), manipulate (optics) and measure (detectors) X-rays for experimental purposes.

All devices used in these experiments are here discussed in some detail. A list of such devices includes:
\begin{itemize}
	\item undulator (source);
	\item slits (optics);
	\item \acl{crl} (optics);
	\item monochromator (optics);
	\item spectrometer (optics);
	\item analyzer (optics);
	\item photodiodes, including an \ac{apd} (detector);
\end{itemize}

Moreover, a general introduction to third-generation synchrotron facilities is also presented, referring to the \ac{esrf} for details.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Synchrotron facilities}
One of the most effective ways of producing X-ray photons is by accelerating charged particles (usually electrons), in specific devices, called \hl{accelerators}, in a \hl{synchrotron facility}. Depending on the design, such a facility can be regarded as first, second, third or fourth generation \ac{sr} installations. Third-generation facilities can be described in terms of few components arranged as shown in \cref{fig:synchrotron}.

The X-ray production scheme can then be summarized in this way:
\begin{enumerate}
	\item an initial beam of electrons is first accelerated by means of a \Hl{linear accelerator}\footnote{A linear accelerator is a device that uses ad-hoc electric fields to increase the speed of charged particles through a linear path.};
	\item such a beam is then injected into a \Hl{booster synchrotron}\footnote{A synchrotron is an accelerator that uses synchronized magnetic fields to progressively increase the speed of charged particles traveling on an almost circular path. It usually consists of linear sections (where beam is focused) alternating with bending sections (where the beam is forced to change its direction).} where it is further accelerated to relativistic speeds;
	\item after that, the electrons are inserted in the \Hl{storage ring}\footnote{Technically, also a storage ring is a synchrotron.}, where they are kept circulating at an almost constant speed for a long period of time;
	\item at various points in the storage ring, electrons emit X-rays (by means of \Hl{bending magnets}\footnote{The bending magnets are used both to achieve the pseudo-circular shape of the storage ring and as X-ray sources, since the electrons are there accelerated to follow a curved trajectory.} and \Hl{undulators});
	\item the produced X-ray beams are then conveyed to the correspondent beamlines, where the experiments take place;
	\item from time to time (usually a few hours), the storage ring (that progressively loses electrons\footnote{Only particles with a specific speed are capable of running unmodified in the storage ring. A number of mechanisms are involved in the degradation of particle kinematical parameters, including energy losses due to X-ray production, border effects of focusing magnets, inter-particle repulsion, etc.}) needs to be refilled by producing a new beam from the linear accelerator all the way up to the storage ring.
\end{enumerate}

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=12.0cm]{figures/synchrotron_scheme.eps}
 \caption[Synchrotron facility]{Schematic representation of the synchrotron facility. The electron beam, produced by the linear accelerator, is brought at relativistic energies by the booster synchrotron and then it is injected into the storage ring, where it is used for the production of the X-rays (by means of insertion devices or bending magnets) subsequently used for the experiments in the beamlines.}
 \label{fig:synchrotron}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Undulator} \label{sec:experiment:undulator}
An undulator is a particular type of insertion device, i.e. a device used for electromagnetic radiation production by means of charged particles acceleration.

The basic structure of an insertion device is a magnetic dipole, that is a pair of opposite magnets close to each other so that the magnetic field between them is, to a large extent, basically constant.
A charged particle traveling inside the dipole gap is deflected (accelerated), according to the electromagnetic (Lorentz) force, thus irradiating energy in the form of electromagnetic waves, i.e. photons.
According to relativistic kinematics, it can be shown that the radiation produced in the dipole has a natural angular divergence of $1/\gamma$, where $\gamma = \frac{1}{\sqrt{1 - \beta^2}}$ and $\beta = \frac{v}{c}$ are the usual relativistic quantities for the charged particle.

An insertion device is a linear array of alternating magnetic dipoles where accelerated charged particles are forced to follow an undulated path.
If the insertion device parameters (both geometrical and electromagnetic) are tuned so that the radiation emitted at each dipole adds up incoherently, the insertion device is called a \Hl{wiggler}.
However, if such parameters allow for coherent superimposition of the emitted electromagnetic radiation (limited to a specific energy), the insertion device is called an \Hl{undulator}.
This naming difference is justified by the very different properties of the output beam.
In particular, the intensity of the emitted radiation scales linearly with the number of dipoles in the case of wigglers, while it scales quadratically for undulators. Moreover, the energy spectrum of undulators is much narrower than that of wigglers.

A necessary condition for an insertion device to operate as an undulator is that the oscillations amplitude of the charged particles should be small with respect to the natural angular divergence $\frac{1}{\gamma}$. This condition can be controlled by the adimensional parameter $K$ that measures the maximum angular deviation from the insertion device axis:
\begin{equation}
	K = \frac{q B \lambda_u}{2\pi m v}
\end{equation}
where $q$, $m$, $v$ are the charge, mass and speed of the particles from which the photons are to be produced, $B$ is the maximum magnetic field acting on the charged particles (it can be tuned by acting on the dipoles distance), and $\lambda_u$ is the dipoles repeating length, also called \hl{undulator wavelength}.

In terms of the parameter $K$:
\begin{itemize}
	\item \hl{undulators} require $K \ll 1$
	\item \hl{wigglers} require $K \gg 1$
\end{itemize}

A summary of the undulator radiation properties (the derivation of which can be found for example in \cite{alsnielsen2011}) is listed below:
\begin{itemize}
	\item the parameters that determine the undulator properties are:
	\begin{itemize}
		\item $K$: the maximum angular deviation from the axis in $\frac{1}{\gamma}$ units;
		\item $\lambda_u$: the undulator wavelength (distance between repeating dipoles);
		\item $N$: the number of periods (half of the number of dipoles);
	\end{itemize}
	\item the wavelength of the radiation emitted by the undulator is $\lambda_1(\theta) = \frac{\lambda_u}{2 \gamma^2} \left( 1 + \frac{K^2}{2} + (\gamma \theta)^2 \right)$ where $\theta$ is the angular deviation from undulator axis, and the angular divergence has a \ac{fwhm} given by $\theta^{\text{\acs{fwhm}}}_{\lambda_1} = \frac{1}{\gamma} \sqrt{\frac{2 + K^2}{2 N}}$, and thus it is smaller than $\frac{1}{\gamma}$;
	\item subsequent peaks, corresponding to higher odd harmonics have a \ac{fwhm} proportional to $\frac{1}{n N}$ ($n$ being the order of the harmonic);
	\item since charged particles beams have a finite size, otherwise vanishing even harmonics are actually present.
	\item the intrinsic brilliance of undulator radiation is very high.
\end{itemize}

In these experiments, the source is an undulator designed for X-ray photons production using accelerated electrons, a schematic representation and a picture of which are shown in \cref{fig:undulator}.
Only the radiation from main peak (first harmonic) has been selected.

\begin{figure}[hbtp]
 \centering
	\subfigure[Scheme]{\label{fig:undulator_scheme}\includegraphics[width=13.0cm]{figures/undulator_scheme.eps}}
	\\
	\subfigure[Picture]{\label{fig:undulator_picture}\includegraphics[width=9.0cm]{figures/undulator_picture.eps}}
 \caption[Undulator]{Schematic representation \subref{fig:undulator_scheme} and a picture \subref{fig:undulator_picture} of the used undulator. The electrons (green) are accelerated by the dipole magnets array of $\lambda_u$ periodicity and X-rays are produced. Note that in undulators, the parameters of the device are tuned in such a way that radiation from each single dipole adds up coherently.}
 \label{fig:undulator}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\acl{crl}}
A \acf{crl} is an X-ray optical device consisting of a linear series of individual lenses to achieve focusing of electromagnetic radiation in the X-ray energy region.

In general, a lens is a ray\footnote{In this context, with the term \hl{ray}, it is intended the trajectory of a moving particle, or, equivalently, the direction of propagation of a plane wave.} manipulating device that can be used to tune the kinematical parameters of a beam, for example the convergence (or collimation).
%Usually, in order to treat different particles, it is required to adopt specific ray manipulating techniques and devices.
For optical lenses, i.e. intended to operate with electromagnetic radiation, \hl{refraction} is exploited, with an ad-hoc shaping of the device that implies interaction with light at specific points in space.

A refractive optical lens consists of an homogeneous substance (or medium) enclosed between a pair of curved \hl{interfaces}\footnote{Here \hl{interface} is intended to be the surface that acts as boundary between two adjacent homogeneous media}.
Indeed, the light passing through an interface is \hl{refracted}, i.e. its speed is modified from one side to the other of the interface.
The ratio between the speed of light in vacuum and the phase velocity of light (at a given energy) in a medium is called the \hl{refractive index}\footnote{Actually the refractive index can be defined to be complex, and its imaginary part describes absorption.} (or \hl{index of refraction}) $n$ and ray the tracing aspects of refraction are well described in terms of this index via the refraction (or Snell's) law\footnote{The refraction law states that the angle of incidence $\vtheta_i$ of a ray with respect to the interface at incidence point, and the angle of the refracted ray $\vtheta_r$ (again with respect to interface) are related to the refractive indexes $n_1$ and $n_2$ of first and second media, respectively, by the relation: $n_1 \bcos{\vtheta_i} = n_2 \bcos{\vtheta_r}$.}.
The parameter governing the convergence properties of a (thin\footnote{A lens is considered thin if the distance traveled by a beam between the interfaces is small compared to its focal length.}) lens is the \hl{focal length}\footnote{A convention on the sign of the focal length can be used to distinguish between converging and diverging lenses: for an input parallel (i.e. \hl{collimated}) beam, converging lenses focus, while diverging lenses spread, the output beam.} $f$, and can be defined as the distance at which an input parallel beam is focused (the output rays intersect)\footnote{In fact, the convergence property of a thin lens is adequately described by the lens equation: $\frac{1}{f} = \frac{1}{p} + \frac{1}{q}$, where $f$ is the focal length, $q$ is the source-lens distance and $p$ is the lens-image distance. For a collimating lens, $q = \infty$ and therefore $p = f$.}: a shorter focal length corresponds to a higher converging power.
The focal length $f$ for a (thin and symmetric\footnote{A lens is considered symmetric if it has a symmetry axis perpendicular to the beam direction.}) lens can be expressed in terms of the curvature radius of the interfaces $R$ and its refractive index $n$:
\begin{equation}
	f = \frac{R}{2 (1 - n)}
\end{equation}

In order to efficiently modify the convergence of a beam, it is required that a lens:
\begin{itemize}
	\item is almost transparent, i.e. light should be able to pass through it with the smallest possible absorption;
	\item the relative refractive index\footnote{Relative to the medium in which the lens is used, usually air.} should be significantly different from $1$ in order to have a high optical power.
\end{itemize}

In the X-ray energy region, while transparency to electromagnetic radiation (low absorption) is achieved by using light elements (small atomic number $Z$) like beryllium ($\chem{Be}$, $Z = 4$), the refractive index differs from $1$ by at most a $10^{-5}$ factor for most materials (note also that $n < 1$) and therefore the optical power is limited.

In order to increase the converging power, multiple single lenses have been arranged in a linear array as shown in \cref{fig:crl}. The resulting focal length reads:
\begin{equation}
	f = \frac{R}{2 N (1 - n)}
\end{equation}
where $R$ is the curvature radius of a single lens interface (equal to the radius of the holes), $N$ is the number of lenses (equivalent to the number of holes or, more precisely, half of the number of curved interfaces) and $n$ is the refractive index.

\begin{figure}[hbtp]
 \centering
	\subfigure[Scheme]{\includegraphics[width=14cm]{figures/crl_scheme.eps}}
	\\
	\subfigure[Picture]{\includegraphics[width=9cm]{figures/crl_picture.eps}}
 \caption[\acl{crl}]{Schematic representation (top) and a picture (bottom) of a typical Beryllium \acl{crl}. Note that since $n < 1$, the converging geometry for an X-ray lens is a concave lens and the diverging geometry is convex, while for optical lenses, having $n > 1$, the opposite holds.}
 \label{fig:crl}
\end{figure}

The parameters of the \ac{crl} used in this experiment to collimate the beam are summarized in \cref{tab:crl_param}.

\begin{table}[hbtp]
	\centering
\begin{tabular}{lc}
\hline \hline
\Hl{Parameter}										& \Hl{Value}						\\
\hline
Material											& Beryllium, $\chem{Be}$ ($Z = 4$)	\\
$\delta$: refractive index deviation (real part)	& $5.45\e{-6}$						\\
$R$: lens curvature radius (hole radius)			& $1\ \um{mm}$						\\
$N$: number of lenses (or holes)					& $3$								\\
$d$: lens width (inter-holes distance)				& $0.1\ \um{mm}$					\\
$f$: focal length									& $30.6\ \um{m}$					\\
\hline \hline
\end{tabular}
	\caption[\acl{crl} parameters]{Parameters of Beryllium \acl{crl} used in the experiment. Note that the real parameter $\delta$ is defined by $n = 1 - \delta - \im \beta$, where $n$ is the complex refractive index. The reported value corresponds to the experimental incident energy ($\approx 8\ \um{keV}$).}
	\label{tab:crl_param}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Monochromator} \label{sec:experiment:monochromator}
A monochromator is an optical device used to select radiation in a narrow energy range.

For electromagnetic radiation, monochromators\footnote{Optical monochromators can be based either on refraction or diffraction effects.} are usually built using a diffraction grating, i.e. a periodic arrangement of scattering elements (segments, planar surfaces, etc.).
The Scattered radiation of a specific wavelength interferes constructively, according to the geometrical properties of the grating (particularly spacing), thus selecting its correspondent energy.

Monochromators can be characterized essentially by two parameters:
\begin{enumerate}
	\item \Hl{passing band}: the energy interval for which the output beam has an intensity much higher than elsewhere;
	\item \Hl{cutoff level}: the intensity of the beam outside the passing band.
\end{enumerate}

In order to improve the cutoff level it is customary to arrange multiple monochromators in series (all selecting the same energy range), because the resulting cutoff level is the product of the cutoff levels of the single monochromators.

A diffracting monochromator intended to work with X-rays must have a grating spacing within the X-ray wavelength range.
Crystals can be used as such gratings because of the size of the lattice parameters.

For the experiments here discussed, a double monochromator setup has been used, shown in \cref{fig:monochromator}.
It has been tuned to an output energy around $7907\ \um{eV}$: more precisely the swept energy range was $\approx \range{7905}{8110}\ \um{eV}$.
Each monochromator is made of a pure silicon single crystal symmetrically cut along the $\miller{1}{1}{1}$ direction (hence using $\chem{Si}{\miller{1}{1}{1}}$ reflection).
The changes in the selected energy are achieved through simultaneous rotations of two crystals.

It has also to be noted that such a device is subject to a high heat load because of the X-ray beam and, in order to have a stable output, it is kept around cryogenic temperatures ($\approx 77\ \um{K}$ close to the temperature at which nitrogen boils) to minimize the thermal bump.
In fact at around such temperatures, the thermal expansion coefficient of silicon is very small and hence the lattice parameter of the crystal (near where the beam strikes) does not change significantly with temperature.

\begin{figure}[hbtp]
 \centering
	\subfigure[Scheme]{\label{fig:monochromator_scheme}\includegraphics[width=8.5cm]{figures/monochromator_scheme.eps}}
	\hspace{0.4cm}
	\subfigure[Picture]{\label{fig:monochromator_picture}\includegraphics[width=6.5cm]{figures/monochromator_picture.eps}}
 \caption[Monochromator]{Schematic representation \subref{fig:monochromator_scheme} and a picture \subref{fig:monochromator_picture} of the used monochromator. The double crystal setup is used in order to improve the signal cutoff level.}
 \label{fig:monochromator}
\end{figure}

Since in the experiments here addressed, the monochromator is the last optical element before the sample, it should be properly considered when modeling the incident beam as seen by the sample.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Spectrometer} \label{sec:experiment:spectrometer}
The spectrometer is an energy selecting device consisting of a monochromator, called \hl{crystal analyzer} in this context, coupled with an appropriate detector, and arranged in an ad-hoc spatial configuration.

For the experiments here discussed, a spectrometer in \Hl{Rowland} geometry has been used as shown in \cref{fig:spectrometer}.
It consists of a concave spherical analyzer with a specific radius of curvature $R$, allowing for energy selection and focusing at the same time.
In fact, it can be shown by simple geometrical considerations that, if the source (sample), the detector and the analyzer all lay on a circle (Rowland Circle) of given diameter $R$ (the same size as the radius of curvature of the analyzer), all of the radiation incident onto the analyzer is diffracted on the same point on the circle, where the detector is placed.
The spectral analysis can be performed by rotating the analyzer and moving the detector so that they always respect Rowland conditions.
In the experiments here discussed, the Bragg angle of the crystal analyzer, and therefore the scattered energy, are kept fixed and the energy scans are performed by tuning incident energy.

\begin{figure}[hbtp]
 \centering
	\subfigure[Scheme]{\label{fig:spectrometer_scheme}\includegraphics[width=7.5cm]{figures/spectrometer_scheme.eps}}
	\hspace{0.4cm}
	\subfigure[Picture]{\label{fig:spectrometer_picture}\includegraphics[width=7.5cm]{figures/spectrometer_picture.eps}}
 \caption[Spectrometer]{Schematic representation \subref{fig:spectrometer_scheme} and a picture \subref{fig:spectrometer_picture} of the used spectrometer. The Rowland geometry is used to analyze and focus the signal beam at the same time.}
 \label{fig:spectrometer}
\end{figure}

The crystal analyzers are made of silicon and the crystal shape ensures that all of the analyzer's surface satisfies the Bragg's condition at the same time (because of the Rowland geometry).
A more detailed discussion of this device is given in \cref{sec:experiment:analyzer}.

In these experiments, it has been used a spectrometer in Rowland geometry, shown in \cref{fig:spectrometer}, whose main properties have been summarized in \cref{tab:spectrometer}.

\begin{table}[hbtp]
	\centering
\begin{tabular}{lc}
\hline \hline
\Hl{Property}								& \Hl{Value}							\\
\hline
movement (rotation)							& horizontal							\\
arm length (Rowland diameter)				& $1\ \um{m}$							\\
analyzer material and reflection			& $\chem{Si}{\miller{4}{4}{4}}$ bent	\\
detector (photodiode)						& \acs{apd}								\\
signal collected in the experiment			& \acs{ddcs}							\\
\hline \hline
\end{tabular}
	\caption[Spectrometer summary]{Summary of the parameters of the spectrometer used for the experiment.}
	\label{tab:spectrometer}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Analyzer} \label{sec:experiment:analyzer}
An analyzer or, more precisely, a crystal analyzer is a monochromator (see \cref{sec:experiment:monochromator}) used in conjunction with a detector to be used as a spectrometer (see also \cref{sec:experiment:spectrometer}).

In the Rowland geometry used for the spectrometer, the size of the analyzer determines the angular acceptance and hence the exchanged momentum resolution.
Clearly, the larger the analyzer, the higher is the measured signal.

The analyzer used in the experiments, shown in \cref{fig:analyzer}, consists of a spherically bent silicon crystal cut along the $\chem{Si}{\miller{4}{4}{4}}$ planes. It has been built gluing a silicon wafer on a spherically curved substrate.
The energy resolution of this analyzer is given by:
\begin{equation}
	\frac{\Delta E}{E} = \frac{2 T}{R} \Big( \bcot{\vtheta_B}^2 - \nu \Big)
\end{equation}
where $T$ is the absorption length of the crystal, $R$ is the curvature radius, $\vtheta_B$ is the Bragg's angle and $\nu$ is the transverse to axial strain ratio (Poisson ratio).

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=6.0cm]{figures/analyzer_picture.eps}
 \caption[Analyzer]{Picture of the spherically bent crystal analyzer used in the experiment. The used reflection is $\chem{Si}{\miller{4}{4}{4}}$, that implies an almost backscattering geometry.}
 \label{fig:analyzer}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Photodiodes}
A photodiode is a photoelectronic device used to convert electromagnetic radiation into an electric signal (electrical current or voltage).

The basic principle behind photodiodes is the photoelectric effect.
In fact, photodiodes consist of either a p-n junction or a \ac{pin} structure (a wide, lightly doped, almost intrinsic semiconductor region between a p-type semiconductor and an n-type semiconductor regions).
When a photon of suitable energy hits the diode, free electrons are generated.
If such electrons are produced in a region where the electric field within the semiconductor is high enough (e.g. the depletion region of a p-n junction), a photo-current flows.

Photodiodes can operate at different regimes:
\begin{itemize}
	\item \Hl{photovoltaic}: only photoelectrons (and thermally produced electrons) contribute to the current flowing in the diode (zero bias);
	\item \Hl{photoconductive}: a moderate inverse current is applied to the diode (reverse bias) in order to increase the depletion layer and thus decreasing the corresponding capacitance, resulting in a faster response time at the cost of a higher noise;
	\item \Hl{\acs{apd}} (\acl{apd}): the photodiode operates in high reverse bias, thus allowing each photo-electron to be multiplied by avalanche breakdown (but below the breakdown voltage), resulting in internal gain within the photodiode and therefore with a much higher sensitivity, but still operating in linear regime, i.e. the signal intensity is proportional to the number of events.
	\item \Hl{Single-Photon \ac{apd}}: if the reverse bias applied to the photodiode is above the breakdown voltage, the device is capable of detecting a single-photon arrival and the signal, whose intensity is no longer linear, is analyzed by counting the number of events.
\end{itemize}

In this experiments, two custom-designed (by Canberra-Eurisys and Perkin-Elmer) photodiode detectors have been used, each operating in a different regime.
Particularly, the detector used in the horizontal spectrometer works as a single-photon \ac{apd} and therefore its noise statistics follows a Poisson distribution (thus the error is estimated to be the square root of the signal counts).
Instead, the detector used to measure the diffracted intensity operates in photovoltaic regime.

% \begin{figure}[hbtp]
%  \centering
%  \includegraphics[width=12.0cm]{figures/photodiode_picture.eps}
%  \caption[Photodiodes]{Picture of photodiodes used in the experiments.}
%  \label{fig:photodiodes}
% \end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
