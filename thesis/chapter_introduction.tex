\chapter{Introduction} \acresetall
% context of the work
% relevance of the work
% objective of the work
% details of the work (chapter by chapter)

A classical problem in condensed matter physics is the determination of the electron dynamics at the atomic scale.
Today's experimental research in this area aims at gaining increasingly detailed information, both for the refinement of theoretical models (e.g. ab initio calculations, etc.) and for the optimization of the properties of novel materials (e.g. in electronic instrumentation, high-technology devices, biological materials, etc.).

At the \ac{esrf} in Grenoble (France), there is an active line of research devoted to the study of electron dynamics in solids and amorphous materials by means of X-ray radiation.
One of the key observables in this context is the \ac{dsf}, determined via \ac{ixs} experiments.
The use of this technique for such measurements has developed very much in recent years thanks to the availability of the unprecedented brilliance of the X-ray beams produced at third generation synchrotron sources like the ESRF.

However, the \ac{dsf} determination only gives spatially averaged information on the electron dynamics of the system under investigation, as it will be explained in some details later in this work. It has been demonstrated, both theoretically in \cite{schuelke1981,golovchenko1981,schuelke1982} and experimentally in \cite{schuelke1986}, that it is possible to use a specific technique, called \ac{cixs}, to gain more detailed information on the electron dynamics of a material at the atomic scale. Such a technique requires an ad-hoc engineering of the initial photon state, with a spatial modulation at the scale of interest. Such modulation can be achieved by the superposition of two plane waves in a perfect crystal sample by realizing an \ac{ixs} experiment on a sample  where a standing wave field is induced. In \cite{schuelke1991}, \ac{cixs} experiments have been reported to measure the \ac{ndsf} of silicon and also to give direct evidence of the existence of plasmon bands.

To date, the cited experiments on silicon are the only \ac{cixs} ones performed so far.
In fact, not only the experiment is very demanding in terms of setup and data analysis, but also it is limited to perfect crystalline samples in order to produce a suitable incident photon state.

However, given the present availability of coherent X-ray sources at the \ac{xfel} facilities, this technique may be applied today, with proper generalization, also to a broader class of systems and it is therefore of interest to reconsider it at this stage.

The goal of the present work is to evaluate the limits of the already used data analysis techniques for measurements of the \ac{ndsf} in perfect crystals like silicon and to explore a new method of performing the \ac{cixs} data analysis.
This is a first relevant step for a future application of \ac{cixs} at \ac{xfel} sources.

The work for this thesis consisted mainly in the development of new computational tools for data analysis of measurements collected separately\footnote{Not by the author.}.

This thesis is structured into five more chapters:
\begin{enumerate}
	\item the \hl{first} chapter is this introduction;
	\item the \hl{second} chapter is intended to present the theoretical foundations of \acf{cixs}, including a derivation of the \acf{ddcs} formula in terms of the \ac{dsf} (or, equivalently, in terms of the dielectric function) and other parameters, the explanation of which requires a brief discussion of the \acf{dtxrd};
	\item the \hl{third} chapter consists of an introduction to the experiments, both in terms of the system under study and in terms of the experimental setup; moreover, a discussion of the relevant instrumentation used for this experiment is also reported;
	\item the \hl{fourth} chapter describes in some details the procedure used to analyze the collected data: the preprocessing (e.g. elastic peak subtraction, background removal, etc.), a first analysis method based on \acf{ias} (previously used for similar experiments), a second analysis method based on \acf{svd} (applied to this technique for the first time), and a comparison of the two methods;
	\item the \hl{fifth} chapter presents and discusses the obtained results; particularly, the results of experiments repeated under the same experimental conditions are compared to verify their accuracy, and are also compared with the results available in the literature; additionally, a direct experimental evidence of the plasmon bands model is presented.
	\item the \hl{sixth} chapter concludes this work by summarizing the results obtained and discussing possible new directions for these class of experiments, particularly exploiting the recently available coherent X-ray beams produced by \acp{xfel}.
\end{enumerate}
