\chapter{Discussion of results} \label{sec:results} \acresetall
In this chapter the obtained results will be presented and discussed. Therefore, it contains:

\begin{itemize}
	\item a list of the experimental parameters for the analyzed experimental runs;
	\item the obtained \ac{ddsf} and \ac{ndsf} (and related results) normalized spectra for each experimental run;
	\item a comparison of experimentally equivalent runs;
	\item a comparison with literature results (if available);
	\item the evidence of plasmon bands.
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Experiments}
A number of measurements under different experimental conditions have been performed, as summarized in \cref{tab:expt_run}.
In particular, it has been specified, for each experimental run: the lattice vector $\vect{g}$ corresponding to the Bragg condition selected to build the standing wave field in the sample; the exchanged momenta $\vect{q}_0$ and $\vect{q}_h$; the $\babs{\vect{q}_0}/\babs{\vect{q}_c}$ ratio, where $\babs{\vect{q}_c}$ is the critical value at which the dispersion relation of the plasmon band meets the electron-hole pairs section\footnote{A more detailed discussion of this parameter is presented in \cref{sec:results:plasmons}}.

\begin{table}[hbtp]
	\centering
\begin{tabular}{cccccl}
\hline \hline
\Hl{$\vect{g}$}		& \Hl{$\vect{q}_0$}				& $\vect{q}_h$					& \Hl{$\babs{\vect{q}_0}/\babs{\vect{q}_c}$}	& $q_0$ ($\um{a.u.}$)	& \Hl{Runs (Codes)}				\\
\hline
$\miller{1}{1}{1}$	& $-0.1 \x \miller{5}{6}{4}$	& $0.1 \x \miller{5}{4}{6}$		& $0.85$										& 0.54					& \# 1, 2 (hkl456\_[1,2])		\\
$\miller{1}{1}{1}$	& $-0.5 \x \miller{-1}{3}{1}$	& $0.5 \x \miller{3}{-1}{1}$	& $1.61$										& 1.02					& \# 3, 4 (hkl113\_[1,2])		\\
$\miller{1}{1}{1}$	& $-0.25 \x \miller{-3}{7}{2}$	& $0.25 \x \miller{7}{-3}{2}$	& $1.92$										& 1.21					& \# 5, 6 (hkl327\_[1,2])	\\
\hline \hline
\end{tabular}
	\caption[Summary of the experiments]{Summary of experimental runs and related experimental parameters. Note that $\vect{q}_0$ and $\vect{q}_h$ are always symmetry equivalents.}
	\label{tab:expt_run}
\end{table}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{The results}
The results obtained for the \ac{dsf} are hereby presented, along with a few checks: the check on the stability of measurements by comparing similar runs, and the check on first spectral moment\footnote{The first moment of a function $f(x)$ is given by the integral: $I = \defint{x \x f(x)}{x}{-\infty}{+\infty}$.} as predicted by the theory (see also \cref{sec:data_analysis:absolute_scale}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The extraction of the \acl{dsf} and its normalization}
The results for the normalized \acl{dsf} spectra obtained via the \ac{svd} method (see \cref{sec:data_analysis:dsf}) and the correspondent experimental preprocessed data (see \cref{sec:data_analysis:preprocess}), are presented in the figures according to \cref{tab:results}.

\begin{table}[hbtp]
	\centering
\begin{tabular}{ccccc}
\hline \hline
\Hl{Run}	& \Hl{Code}		& \Hl{Figure}				\\
\hline
1			& (hkl456\_1)	& \ref{fig:dsf_hkl456_1}	\\
2			& (hkl456\_2)	& \ref{fig:dsf_hkl456_2}	\\
\hline
3			& (hkl113\_1)	& \ref{fig:dsf_hkl113_1}	\\
4			& (hkl113\_2)	& \ref{fig:dsf_hkl113_2}	\\
\hline
5			& (hkl327\_1)	& \ref{fig:dsf_hkl327_1}	\\
6			& (hkl327\_2)	& \ref{fig:dsf_hkl327_2}	\\
% 7			& (hkl327\_3)	& \ref{fig:dsf_hkl327_3}	& \pageref{fig:dsf_hkl327_3}	\\
\hline \hline
\end{tabular}
	\caption[Summary of the experimental results]{Summary of the experimental results presented for each run.}
	\label{tab:results}
\end{table}

\begin{figure}[hbtp]
 \centering
	\subfigure[input data]{\label{fig:dsf_hkl456_1_data}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl456_1_input_data_surfc.eps}}
	\hspace{0.4cm}
	\subfigure[\ac{dsf}]{\label{fig:dsf_hkl456_1_spectrum}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl456_1_norm_dsf.eps}}
 \caption[Input data and \acl{dsf} results of run 1 (hkl456\_1)]{Preprocessed input data \subref{fig:dsf_hkl456_1_data} and \ac{dsf} results \subref{fig:dsf_hkl456_1_spectrum} for the experimental run 1 (hkl456\_1).}
 \label{fig:dsf_hkl456_1}
\end{figure}

\begin{figure}[hbtp]
 \centering
	\subfigure[input data]{\label{fig:dsf_hkl456_2_data}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl456_2_input_data_surfc.eps}}
	\hspace{0.4cm}
	\subfigure[\ac{dsf}]{\label{fig:dsf_hkl456_2_spectrum}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl456_2_norm_dsf.eps}}
 \caption[Input data and \acl{dsf} results of run 2 (hkl456\_2)]{Preprocessed input data \subref{fig:dsf_hkl456_2_data} and \ac{dsf} results \subref{fig:dsf_hkl456_2_spectrum} for the experimental run 2 (hkl456\_2).}
 \label{fig:dsf_hkl456_2}
\end{figure}

\begin{figure}[hbtp]
 \centering
	\subfigure[input data]{\label{fig:dsf_hkl113_1_data}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl113_1_input_data_surfc.eps}}
	\hspace{0.4cm}
	\subfigure[\ac{dsf}]{\label{fig:dsf_hkl113_1_spectrum}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl113_1_norm_dsf.eps}}
 \caption[Input data and \acl{dsf} results of run 3 (hkl113\_1)]{Preprocessed input data \subref{fig:dsf_hkl113_1_data} and \ac{dsf} results \subref{fig:dsf_hkl113_1_spectrum} for the experimental run 3 (hkl113\_1).}
 \label{fig:dsf_hkl113_1}
\end{figure}

\begin{figure}[hbtp]
 \centering
	\subfigure[input data]{\label{fig:dsf_hkl113_2_data}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl113_2_input_data_surfc.eps}}
	\hspace{0.4cm}
	\subfigure[\ac{dsf}]{\label{fig:dsf_hkl113_2_spectrum}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl113_2_norm_dsf.eps}}
 \caption[Input data and \acl{dsf} results of run 4 (hkl113\_2)]{Preprocessed input data \subref{fig:dsf_hkl113_2_data} and \ac{dsf} results \subref{fig:dsf_hkl113_2_spectrum} for the experimental run 4 (hkl113\_2).}
 \label{fig:dsf_hkl113_2}
\end{figure}

\begin{figure}[hbtp]
 \centering
	\subfigure[input data]{\label{fig:dsf_hkl327_1_data}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl327_1_input_data_surfc.eps}}
	\hspace{0.4cm}
	\subfigure[\ac{dsf}]{\label{fig:dsf_hkl327_1_spectrum}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl327_1_norm_dsf.eps}}
 \caption[Input data and \acl{dsf} results of run 5 (hkl327\_1)]{Preprocessed input data \subref{fig:dsf_hkl327_1_data} and \ac{dsf} results \subref{fig:dsf_hkl327_1_spectrum} for the experimental run 5 (hkl327\_1).}
 \label{fig:dsf_hkl327_1}
\end{figure}

\begin{figure}[hbtp]
 \centering
	\subfigure[input data]{\label{fig:dsf_hkl327_2_data}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl327_2_input_data_surfc.eps}}
	\hspace{0.4cm}
	\subfigure[\ac{dsf}]{\label{fig:dsf_hkl327_2_spectrum}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl327_2_norm_dsf.eps}}
 \caption[Input data and \acl{dsf} results of run 6 (hkl327\_2)]{Preprocessed input data \subref{fig:dsf_hkl327_2_data} and \ac{dsf} results \subref{fig:dsf_hkl327_2_spectrum} for the experimental run 6 (hkl327\_2).}
 \label{fig:dsf_hkl327_2}
\end{figure}

% \begin{figure}[hbtp]
%  \centering
% 	\subfigure[input data]{\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl327_3_input_data_surfc}}
% 	\hspace{0.4cm}
% 	\subfigure[\ac{dsf}]{\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl327_3_norm_dsf}}
%  \caption[Input data and \acl{dsf} results of run 7 (hkl327\_3)]{Preprocessed input data \subref{fig:dsf_hkl456_1_data} and \ac{dsf} results \subref{fig:dsf_hkl456_1_spectrum} for the experimental run 7 (hkl327\_3).}
%  \label{fig:dsf_hkl327_3}
% \end{figure}

All these spectra present a peak for the \ac{ddsf} and a peak-valley structure for the \ac{ndsf}.
Clearly, as the modulus of the momentum $\vect{q}_0$ ($=\vect{q}_h$) increases, the energy position of the peak of the \ac{ddsf} also increases.

It has to be noted that the first two runs present a smooth shape, while all other experimental runs do have a significantly larger high-frequency noise.
Such a noise is also present in the input data and its origin is uncertain.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The reproducibility check}
In order to check for the reproducibility of the \ac{dsf} extraction procedure, the results relative to measurements taken under equivalent experimental condition have been compared.
Such comparison, presented in \cref{fig:hkl456_comp}, \cref{fig:hkl113_comp}, \cref{fig:hkl327_comp}, indicates that results are reasonably reproducible.

Averaged spectra have also been calculated and are shown in \cref{fig:hkl456_avg}, \cref{fig:hkl113_avg}, \cref{fig:hkl327_avg}.

\begin{figure}[hbtp]
 \centering
	\subfigure[Superimposed \acs{dsf} spectra]{\label{fig:hkl456_comp}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl456_comp.eps}}
	\hspace{0.4cm}
	\subfigure[Averaged \acs{dsf} spectra]{\label{fig:hkl456_avg}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl456_average.eps}}
 \caption[\acl{dsf} comparison and average at $\vect{q}_0 = -0.1 \miller{5}{6}{4}$]{\acl{dsf} comparison and the corresponding average for the measurements at $\vect{q}_0 = -0.1 \miller{5}{6}{4}$.}
 \label{fig:hkl456_comp_avg}
\end{figure}

\begin{figure}[hbtp]
 \centering
	\subfigure[Superimposed \acs{dsf} spectra]{\label{fig:hkl113_comp}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl113_comp.eps}}
	\hspace{0.4cm}
	\subfigure[Averaged \acs{dsf} spectra]{\label{fig:hkl113_avg}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl113_average.eps}}
 \caption[\acl{dsf} comparison and average at $\vect{q}_0 = -0.5 \miller{-1}{3}{1}$]{\acl{dsf} comparison and the corresponding average for the measurements at $\vect{q}_0 = -0.5 \miller{-1}{3}{1}$.}
 \label{fig:hkl113_comp_avg}
\end{figure}

\begin{figure}[hbtp]
 \centering
	\subfigure[Superimposed \acs{dsf} spectra]{\label{fig:hkl327_comp}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl327_comp.eps}}
	\hspace{0.4cm}
	\subfigure[Averaged \acs{dsf} spectra]{\label{fig:hkl327_avg}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl327_average.eps}}
 \caption[\acl{dsf} comparison and average at $\vect{q}_0 = -0.25 \miller{-3}{7}{2}$]{\acl{dsf} comparison and the corresponding average for the measurements at $\vect{q}_0 = -0.25 \miller{-3}{7}{2}$.}
 \label{fig:hkl327_comp_avg}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The absolute scale check} \label{sec:results:abs_scale}
A strong check for the accuracy of the results is offered by the comparison of the \ac{ddsf} and the \ac{ndsf} absolute scale normalizations:

\begin{align}
 I_d = & \defint{E \x S(\vect{q}, E)}{E}{0}{+\infty} = \frac{\hbar^2}{2 m_e} \vect{q}^2 \\
 I_n = & \defint{E \x S(\vect{q}, \vect{g}, E)}{E}{0}{+\infty} = \frac{\hbar^2}{2 m_e} \left[ \vect{q} \sprod (\vect{q} + \vect{g}) \right] \left( \frac{\rho_{\vect{g}}}{\rho_0} \right)
\end{align}

where $I_d$ and $I_n$ are first spectral moment\footnote{Sometimes this is called also the sum rule for \ac{dsf}.} for the \ac{ddsf} and \ac{ndsf}, respectively.

Indeed, the ratio between these two normalization factors reads:
\begin{equation}
	\frac{I_n}{I_d} = \frac{\vect{q} \sprod (\vect{q} + \vect{g})}{\vect{q} \sprod \vect{q}} \frac{\rho_{\vect{g}}}{\rho_0}
\end{equation}
which can be rewritten, after isolating the $\frac{\rho_{\vect{g}}}{\rho_0}$ quantity, in terms of the experimental momenta $\vect{q}_0$ and $\vect{q}_h$:
\begin{equation}
	\frac{\rho_{\vect{g}}}{\rho_0} = \frac{I_n}{I_d} \frac{\vect{q} \sprod \vect{q}}{\vect{q} \sprod (\vect{q} + \vect{g})} = \frac{I_n}{I_d}  \frac{\vect{q}_0 \sprod \vect{q}_0}{\vect{q}_0 \sprod \vect{q}_h}
\end{equation}

In \cref{tab:rho_ratio}, the calculation of such a ratio for each experimentally measured \ac{dsf} spectra is reported and compared with the expected calculated values.

\begin{table}[hbtp]
	\centering
\begin{tabular}{ccccc}
\hline \hline
\Hl{Runs}	& \Hl{Code}	& \Hl{Measured $\rho_{\vect{g}} / \rho_0$}	& \Hl{Calculated $\rho_{\vect{g}} / \rho_0$}	& \Hl{Compatible}	\\
\hline
1, 2		& (hkl456)	& $0.20(3)$									& $0.221(6)$									& \checkYes			\\
3, 4		& (hkl113)	& $0.07(6)$									& $0.221(6)$									& \checkNo			\\
5, 6		& (hkl327)	& $0.23(3)$									& $0.221(6)$									& \checkYes			\\
\hline \hline
\end{tabular}
	\caption[Measured and literature $\rho$ ratio values]{Measured and calculated $\rho$ ratio values (see \cite{kaprolat1991}). Note that calculated value is always the same because $\vect{g}$ does not change for all experimental runs.}
	\label{tab:rho_ratio}
\end{table}

First and last experimental results have the measured $\rho$ ratio values compatible with the calculated one.
It is not clear why some experimental results do not reproduce the calculated value.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Comparison with the literature}
Some of the experimental results can be compared with results from previous studies.

In particular, the measurements at $\vect{q}_0 = -0.1 \miller{5}{6}{4}$ can be compared with the results from \cite{kaprolat1991}, as shown in \cref{fig:hkl456_literature}.

\begin{figure}[hbtp]
 \centering
	\subfigure[Direct comparison]{\label{fig:hkl456_literature_raw}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl456_lit_comp.eps}}
	\hspace{0.4cm}
	\subfigure[Comparison after shift]{\label{fig:hkl456_literature_shift}\includegraphics[width=7.5cm, trim=0em -1em 0em 0em]{figures/data_analysis/hkl456_lit_comp_shift.eps}}
 \caption[Comparison with literature for (hkl456) runs]{\acl{dsf} comparison with literature data (digitized from results presented in \cite{kaprolat1991}) for measurements at $\vect{q}_0 = -0.1 \miller{5}{6}{4}$. The data from the literature appear shifted as compared with the present results \subref{fig:hkl456_literature_raw}. The compatibility between the two sets of data is obtained by shifting the literature data by $\approx 2\ \um{eV}$ \subref{fig:hkl456_literature_shift}.}
 \label{fig:hkl456_literature}
\end{figure}

While the shape of the spectra is compatible with the results in the literature, an energy transfer shift of $\approx 2\ \um{eV}$ or, equivalently, a $\approx 1.1$ magnification factor has to be applied to the literature data to gain full compatibility with present results.
The origin of this difference is uncertain.
Note that a similar shift between recent \ac{ddsf} data on silicon has also been reported in \cite{weissker2010}.

As an additional test, the maximum of the \ac{ddsf} spectra is plotted, in \cref{fig:silicon_111_dispersion}, against the dispersion of the \ac{dsf} of silicon measured, in \cite{schuelke1995}, along the $\miller{1}{1}{1}$ direction by \ac{ixs}.

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=12cm]{figures/silicon_111_dispersion.eps}
 \caption[Dispersion of the \acl{dsf} for $\chem{Si}\miller{1}{1}{1}$]{Dispersion of the \ac{dsf} in $\chem{Si}\miller{1}{1}{1}$, including current measurements and values from the literature (\cite{schuelke1995}). The modulus of exchanged momentum is in atomic units. Note that the dotted line is only presented as a visual guide.}
 \label{fig:silicon_111_dispersion}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Evidence of plasmon bands} \label{sec:results:plasmons}
In the so-called jellium model, the valence electrons behavior in solids is described approximately, within the framework of quantum mechanics\footnote{It is also possible to introduce a semi-classical model that gives many results consistent with the quantum mechanical formulation.}, by a homogeneous electron gas in a uniform potential, accounting for the electromagnetic interaction of the electrons with the atomic nuclei and  the electrons themselves.

Within the jellium model, it is possible to discuss the existence of collective oscillations of the electron gas density (with respect to the positively charged ions in the solid) called plasmons.

However, this model is an oversimplification of the behavior of electrons in solids (crystals), because the lattice structure of course induces a periodic potential.
It is well known that such a periodic potential leads, in solids, to a band structure in the electron energy levels.
It can be shown that a similar band structure arises for plasmons as well.
In the case of silicon, a two plasmons band model is often introduced, as shown in \cref{fig:plasmon_bands}.

\begin{figure}[hbtp]
 \centering
 \subfigure[Representation of $\vect{q}_c$]{\label{fig:plasmon_cutoff}\includegraphics[width=7.5cm]{figures/plasmon_cutoff.eps}}
 \hspace{0.4cm}
 \subfigure[Sketch of two plasmon band model]{\label{fig:plasmon_bands}\includegraphics[width=7.5cm]{figures/plasmon_bands.eps}}
 \caption[Plasmon bands model]{Schematic representation of a plasmon bands model. In \subref{fig:plasmon_cutoff} it is shown the plasmon cutoff wavevector $\vect{q}_c$, beyond which plasmons are over-dumped as they decay into electron-hole pairs (in light red in the picture). In \subref{fig:plasmon_bands} it is shown a sketch of a two plasmon band model, with the upper and lower plasmon frequencies $\omega_u$ and $\omega_l$ separated by a gap.}
 \label{fig:plasmons}
\end{figure}

It is somewhat hard to find a direct evidence for the existence of such plasmon bands as predicted by the theory, one of the reasons being that plasmons are usually broad.
A typical plasmon dispersion curve is shown in \cref{fig:plasmon_cutoff}, where it is also indicated the plasmon cutoff value.

The experimental conditions explored in this work allow for a direct observation of plasmon bands in silicon, as described in more details in \cite{schuelke2007}, and some of their properties can be measured, like the energy gap at the so-called L-point (i.e. the high-symmetry point in the reciprocal space identified by $\vect{g}_L = \frac{1}{2}\miller{1}{1}{1}$).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{\acl{dsf} and plasmon bands}
Since the $\vect{g}_L = \frac{1}{2}\miller{1}{1}{1}$ point in silicon satisfies the $\babs{\vect{g}_L} < \babs{\vect{q}_c}$ condition, plasmon bands are expected to be sharp enough to be observable.

If one prepares a \ac{cixs} in silicon such that both the $\vect{q}_0$ and $\vect{q}_h$ vectors are related to the L-point by the $\vect{q}_0 = \vect{q}_h \approx \vect{g}_L$ relation and $\vect{q}_0$ lies within the first Brillouin zone, the determination of the diagonal and the non-diagonal parts of the electron density-density correlation function, i.e. the \ac{dsf} (at the $\vect{q}_0$ and $\vect{q}_0 + \vect{g}$ exchanged momentum points), gives direct information on the plasmon band structure.

In particular, within the two plasmon bands model, it is possible to show (see \cite{schuelke2007}) that plasmon bands are related to the \ac{dsf} via the following relations:
\begin{align}
	S_d		& = P_l + P_u\\
	S_n		& = P_l - P_u
\end{align}
where $S_d$ is the \ac{ddsf}, $S_n$ is the \ac{ndsf}, $P_u$ and $P_l$ are the upper and the lower plasmon bands, respectively.

From these relation it is possible to reconstruct the plasmon bands as:
\begin{align}
	P_u	& = \frac{S_d - S_n}{2}\\
	P_l	& = \frac{S_d + S_n}{2}
\end{align}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{The experimental results}
It is possible to calculate the plasmon bands making use of the aforementioned formulas for the \ac{dsf} results obtained in this work.
The result of such operation is show in \cref{fig:hkl456_plasmons}.

\begin{figure}[hbtp]
 \centering
 \includegraphics[width=16cm]{figures/data_analysis/hkl456_plasmons.eps}
 \caption[Plasmons at $\vect{q} \approx \frac{\miller{1}{1}{1}}{2}$]{Plasmons calculated at $\vect{q} \approx \frac{\miller{1}{1}{1}}{2}$ from the averaged \ac{dsf} spectra. The energy difference between the position of the two peaks has been measured to be $\approx 1.5\ \um{eV}$ in this experiment.}
 \label{fig:hkl456_plasmons}
\end{figure}

The position, the shape and the energy gap found in the present experiment are quite compatible with the results from \cite{schuelke2007}, though the energy gap between the two bands is $\approx 1.5\ \um{eV}$ in the current work, while it was found to be $\approx 1.0\ \um{eV}$ in the aforementioned reference.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%